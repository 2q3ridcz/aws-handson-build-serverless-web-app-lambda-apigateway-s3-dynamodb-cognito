# aws-handson-build-serverless-web-app-lambda-apigateway-s3-dynamodb-cognito

## 状況

全て完了。

業務で Aws使うかも…という話はなくなったが、hands-on はやり切った。

## hands-on

[AWS Lambda、Amazon API Gateway、Amazon S3、Amazon DynamoDB、および Amazon Cognito を使用してサーバーレスウェブアプリケーションを構築する方法 | AWS](https://aws.amazon.com/jp/getting-started/hands-on/build-serverless-web-app-lambda-apigateway-s3-dynamodb-cognito/)

> - モジュール  
>     このワークショップは 5 つのモジュールに分かれています。それぞれのモジュールは、これから構築する内容のシナリオとアーキテクチャの実装と作業の検証に役立つステップバイステップの手順について解説します。
>     1. 静的ウェブホスティング
>     2. ユーザー管理
>     3. サーバーレスバックエンド
>     4. RESTful API
>     5. リソースの終了と次のステップ

## 準備

前準備…というより、手順を進めていく中で必要だと判明するものたち。

### IAM User

開発用の IAM Userが必要。

- User
    - アクセスの種類を選択する際は「プログラムによるアクセス」も「AWS マネジメントコンソールへのアクセス」もチェック要。
    - アクセス権限はグループで設定。
- Group
    - 付与したアクセス権限。
        - AWSCodeCommitPowerUser
        - AmazonS3ReadOnlyAccess
            - PublicS3からサンプルコードをダウンロードするために必要。
        - AdministratorAccess-Amplify
            - Amplify コンソールを使用するために必要。
        - AmazonSNSFullAccess
            - Amplify で Deploy するために必要。
        - AmazonDynamoDBFullAccess
            - DynamoDB で CreateTable するために必要。
        - AWSLambda_FullAccess
            - Lambda 関数を作成するために必要。
        - AmazonAPIGatewayAdministrator
            - API を作成するために必要。

### aws-cli

手順の途中で、当たり前のように aws コマンドを使う。  

[amazon/aws-sam-cli-build-image-provided - Docker Image | Docker Hub](https://hub.docker.com/r/amazon/aws-sam-cli-build-image-provided)

PCを汚したくないので、Dockerで環境用意。aws-cli のイメージもあったが、aws-cli と sam-cli の両方が含まれているイメージを使用。

[Configuration basics - AWS Command Line Interface](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html)

そして、このサイトに沿ってユーザ設定。こんな感じ。

```
$ aws configure
AWS Access Key ID [None]: AKIAIOSFODNN7EXAMPLE
AWS Secret Access Key [None]: wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
Default region name [None]: ap-northeast-1
Default output format [None]: json
```

## モジュール 1 - 静的ウェブホスティング

CodeCommit に push すると Amplify で作成したサイトに自動Deploy されることを体験できた。

単体で比較するなら、 CodeCommit + Amplify よりも GitHubPages や GitLabPages の方が楽だと思った。(慣れてるからってのもあるけど。)

あとは他の Awsサービスを使ってて、ユーザ管理をまとめられる…とかがあれば CodeCommit + Amplify を使うメリットが出るかも。

モジュールの内容よりも、IAM User作成やら権限付与やら aws-cliの環境構築やらにめっちゃ時間がかかった… 特に権限は、手順進めてダメな場合に調査して追加してと振り回された感が満載。

## モジュール 2 - ユーザー管理

ユーザアカウントの管理ってセキュリティとかめっちゃ面倒だけど、 Cognito 使うと簡単。

これがあれば、 GitLabPages とかよりも Amplify を使いたい！ と思う。

## モジュール 3 - サーバーレスバックエンド

Lambda って楽ちんだなぁー

DynamoDB はなにに使うんだ…？

## モジュール 4 - RESTful API

Amazon API Gateway は無期限無料ではなく 12 か月間無料なので注意！！

API Gatewayのトップページから「[API の作成] を選択」する手順があるけど、API Gateway のトップページが変わってる…

![./img/api-gateway.png](./img/api-gateway.png)

左下の REST API の 「構築」ボタンを選択した。これ以降はほぼ手順通りで進められた。

途中、「You are not authorized to call DescribeVpcEndpoints」や「ユーザーは、ウェブアプリケーションファイアウォール (WAF リージョン) の ListWebACL および AssociateWebACL 権限を持っていません。WAF 以外のステージ設定は変更できます。」って警告が出たけど、無視して進めても問題は起きなかった。

「CloudWatch Log を削除する」は忘れやすそうだなぁ…

あと、この手順では CodeCommit リポジトリは削除しない。ので、削除したい場合は手探りで削除すること(簡単)。
